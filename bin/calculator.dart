import 'dart:math';

class Calculator {
  List token = [];
  List postfix = [];
  num result = 0;

  Calculator(String ex) {
    this.token = tokenizing(ex);
    this.postfix = infixToPostfix(token);
    this.result = evaluatePostfix(postfix);
  }

  //This function turn string to token
  List tokenizing(input) {
    final _whitespaceRE = RegExp(r"\s+"); //Set regular expressions for multiple whitespace
    String formatedInput = input.replaceAll(_whitespaceRE,' '); //Replace all multiple whitespace to single whitespace
    List tokens = formatedInput.split(' '); //Sprit each token by whitespace
    return tokens;
  }

//This function turn infix to postfix
  List infixToPostfix(List input2) {
    List operators = [];
    List postfix = [];

    input2.forEach((element) {
      var token;
      try {
        token = double.parse(element); //Try to convert token to int.
      } catch (e) {
        //If token is not number do.
        if (isOp(element)) {
          //If token is operators
          while (operators.isNotEmpty &&
              operators.last != '(' &&
              precedence(element) <= precedence(operators.last)) {
            postfix.add(operators.last);
            operators.removeLast();
          }
          operators.add(element);
        }
        if (element == '(') {
          operators.add(element);
        }
        if (element == ')') {
          while (operators.last != '(') {
            postfix.add(operators.last);
            operators.removeLast();
          }
          operators.remove('(');
        }
      }
      if (token is double) {
        //if token is number then add to postfix.
        postfix.add(token);
      }
    });

    while (operators.isNotEmpty) {
      postfix.add(operators.last);
      operators.removeLast();
    }

    return postfix;
  }

//This function evaluate postfix
  num evaluatePostfix(List input3) {
    List values = [];
    var right, left;

    for (var element in input3) {
      if (element is double) {
        //if token is int add to values.
        values.add(element);
      } else {
        right = values.last;
        values.removeLast();
        left = values.last;
        values.removeLast();

        //Implement every operaters.
        if (element == '+') {
          values.add(left + right);
        } else if (element == '-') {
          values.add(left - right);
        } else if (element == '*') {
          values.add(left * right);
        } else if (element == '/') {
          values.add(left / right);
        } else if (element == '^') {
          values.add(pow(left, right));
        } else if (element == '%') {
          values.add((left % right));
        }
      }
    }
    return values.first;
  }

//Function for tell level of precedence.
  int precedence(var op) {
    int level = 0;
    if (op == '^') {
      level = 3;
    } else if (op == '*' || op == '/' || op == '%') {
      level = 2;
    } else if (op == '+' || op == '-') {
      level = 1;
    }
    return level;
  }

//Function for check operater.
  bool isOp(String ele) {
    bool isOp = false;
    if (ele == '+' ||
        ele == '-' ||
        ele == '*' ||
        ele == '/' ||
        ele == '^' ||
        ele == '%') {
      isOp = true;
    } else {
      isOp == false;
    }
    return isOp;
  }
}
